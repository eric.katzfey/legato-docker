
# legato-docker

A docker with the development environment for Legato applications. Specifically
for Sierra WP76xx modules and mangOH Red development board

## Prerequisites

- Docker (Guide here: https://docs.docker.com/engine/install/ubuntu/)

## Set up the mangOH Red

- Register an account on the mangOH Red site
- Follow the mangOH Red setup guide
- You will then be able to log in to the device with:

```$ ssh root@192.168.2.2```

## Building the Docker image

Use the Dockerfile to build a Docker image:

```docker build -t legato .```

## Running the Docker image

This will map your current directory to /opt/workspace/development in the container so start in the directory where your project code is located.

```$ docker run -it --privileged -v`pwd`:/opt/workspace/development --net=host legato```

In the docker:

```# source bin/configlegatoenv```

If you need to update the runtime environment. Only do this once:

```# update build/wp76xx/system.wp76xx.update 192.168.2.2```

## Setting up developer mode

If you haven't enabled developer mode yet:

```
# app install build/wp76xx/tools/devMode.wp76xx.update 192.168.2.2
# app start devMode 192.168.2.2
```

On target:

Modify the config on the target to enable starting devMode upon reboot:

```# config set /apps/devMode/startManual false bool```

## Testing with helloWorld

The following is from https://docs.legato.io/latest/getStartedHW.html

In the docker:

```
# cd /opt/workspace/development/helloWorld
# mkapp -t wp76xx helloWorld.adef
# app install helloWorld.wp76xx.update 192.168.2.2
# app start helloWorld 192.168.2.2
```

On target:

```
logread -f | grep helloWorld
```

## Building legacy applications

As an example, this will compile the file main.c to create an executable
called "main-test". This can be copied to the target and executed there.

In the docker:

```
# cd /opt/swi/SWI9X07Y_02.28.03.05/
# source environment-setup-armv7a-neon-poky-linux-gnueabi
# cd /opt/workspace/development/
# $CC -o main-test main.c
# scp main-test root@192.168.2.2:/home/root
```

On target:

```~# ./main-test```
