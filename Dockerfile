# These steps create the development environment for Legato.
# These were taken from here: https://github.com/legatoproject/legato-af/blob/master/README.md

# Start with Ubuntu 16.04 (Xenial)
FROM ubuntu:xenial

# Add all of the required packages
RUN apt-get update
RUN apt-get install -y autoconf
RUN apt-get install -y automake
RUN apt-get install -y bash
RUN apt-get install -y bc
RUN apt-get install -y bison
RUN apt-get install -y bsdiff
RUN apt-get install -y build-essential
RUN apt-get install -y chrpath
RUN apt-get install -y cmake
RUN apt-get install -y cpio
RUN apt-get install -y diffstat
RUN apt-get install -y flex
RUN apt-get install -y gawk
RUN apt-get install -y gcovr
RUN apt-get install -y git
RUN apt-get install -y gperf
RUN apt-get install -y iputils-ping
RUN apt-get install -y libbz2-dev
RUN apt-get install -y libcurl4-gnutls-dev
RUN apt-get install -y libncurses5-dev
RUN apt-get install -y libncursesw5-dev
RUN apt-get install -y libsdl-dev
RUN apt-get install -y libssl-dev
RUN apt-get install -y libtool
RUN apt-get install -y libxml2-utils
RUN apt-get install -y ninja-build
RUN apt-get install -y python
RUN apt-get install -y python-git
RUN apt-get install -y python-jinja2
RUN apt-get install -y python-pkg-resources
RUN apt-get install -y python3
RUN apt-get install -y texinfo
RUN apt-get install -y unzip
RUN apt-get install -y wget
RUN apt-get install -y vim
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y doxygen
RUN apt-get install -y graphviz
RUN apt-get install -y xsltproc
RUN apt-get install -y repo

WORKDIR /home/root/workspace/legatoAF

# Get the Legato SDK code
RUN repo init -u git://github.com/legatoproject/manifest -m legato/releases/20.04.0.xml
RUN repo sync

WORKDIR /home/root/workspace/legatoAF/legato

# Build the Legato SDK code
# The end of this will fail but it is okay, it fails on an optional step
# because we chose not to install Java
RUN make; exit 0

# Now get and build the toolchain for Sierra WP76xx modules
WORKDIR /home/root/workspace/firmware/wp76xx

RUN wget http://downloads.sierrawireless.com/AirPrime/WP76xx/Release13.3/poky-swi-ext-glibc-x86_64-meta-toolchain-swi-armv7a-neon-toolchain-swi-SWI9X07Y_02.28.03.05.sh
RUN chmod +x poky-swi-ext-glibc-x86_64-meta-toolchain-swi-armv7a-neon-toolchain-swi-SWI9X07Y_02.28.03.05.sh
RUN ./poky-swi-ext-glibc-x86_64-meta-toolchain-swi-armv7a-neon-toolchain-swi-SWI9X07Y_02.28.03.05.sh -y

WORKDIR /opt/swi/SWI9X07Y_02.28.03.05/sysroots/armv7a-neon-poky-linux-gnueabi/usr/src/kernel

RUN export PATH=/opt/swi/SWI9X07Y_02.28.03.05/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi:$PATH && \
    ARCH=arm CROSS_COMPILE=arm-poky-linux-gnueabi- make scripts

WORKDIR /home/root/workspace/legatoAF/legato

RUN make wp76xx

# Now you are ready to develop applications!
CMD ["/bin/bash"]
